package org.moresbycoffee.hackerrank;

import java.util.Scanner;

/**
 * Created by chssyb on 01/10/2015.
 */
public class FlippingBits {

  public static void main(String[] args) {
    FlippingBits flipper = new FlippingBits();

    try(final Scanner scanner = new Scanner(System.in)) {
      final int numOfCases = scanner.nextInt();
      scanner.nextLine();
      for (int i = 0; i < numOfCases; i++) {
        System.out.println(Integer.toUnsignedString(flipper.flip(Integer.parseUnsignedInt(scanner.nextLine()))));
      }
    }
  }


  public int flip(int input) {
    return ~input;
  }
}
