package org.moresbycoffee.hackerrank.algorithms.graphs;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.util.stream.IntStream.range;

/**
 * https://www.hackerrank.com/challenges/bfsshortreach
 */
public class BFSShortestReach {

  // undirected graph #N node
  // S - given node - start position
  // each edge lenght 6 units
  // if the node is unreachable: -1

  //input:
  //first line - number of test cases
  //test case first line: 2 numbers: N number of nodes & M number of edges
  //          m lines: 2 numbers: ends of the edge
  //          last line: 1 number - starting node
  public static void main(final String[] args) {
    final BFSShortestReach bfsShortestReach = new BFSShortestReach();
    try(final Scanner scanner = new Scanner(System.in)) {
      int numOfCase = Integer.parseInt(scanner.nextLine());
      for (int i = 0; i < numOfCase; i++) {
        final String firstLine = scanner.nextLine();
        System.out.println("" + i + "th case: " + firstLine);
        int[] line1 = Arrays.stream(firstLine.split(" ")).filter(s -> !s.isEmpty()).mapToInt(Integer::parseInt).toArray();
        int numberOfNodes = line1[0];
        int numberOfEdges = line1[1];
        int[][] edges = range(0, numberOfEdges)
            .mapToObj(j -> Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray())
            .toArray(int[][]::new);
        int startNode = scanner.nextInt();
        if (i < numOfCase - 1) {
          scanner.nextLine();
        }

        System.out.println(Arrays.stream(bfsShortestReach.shortestReach(numberOfNodes, edges, startNode)).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
      }
    }
  }

  public int[] shortestReach(int numberOfNodes, int[][] edges, int startNodeId) {

    final Map<Integer, Integer> visitedNodes = new TreeMap<>();
    visitedNodes.put(startNodeId, 0);

    while(true) {
      final Map<Integer, Integer> newNodes = new TreeMap<>();
      for (Map.Entry<Integer, Integer> visitedNode : visitedNodes.entrySet()) {
        for (int[] edge : edges) {
          if (edge[0] == visitedNode.getKey() && !visitedNodes.containsKey(edge[1])) {
            newNodes.put(edge[1], visitedNode.getValue() + 6);
          } else if (edge[1] == visitedNode.getKey() && !visitedNodes.containsKey(edge[0])) {
            newNodes.put(edge[0], visitedNode.getValue() + 6);
          }
        }
      }
      visitedNodes.putAll(newNodes);
      if (newNodes.isEmpty()) {
        break;
      }
    }

    return toResultArray(numberOfNodes, startNodeId, visitedNodes);
  }

  private int[] toResultArray(int numberOfNodes, int startNodeId, Map<Integer, Integer> visitedNodes) {
    final int[] result = new int[numberOfNodes - 1];
    for (int i = 1; i <= numberOfNodes; i++) {
      if (i == startNodeId) {
        continue;
      }
      final int pos;
      if (i < startNodeId) {
        pos = i-1;
      } else {
        pos = i-2;
      }
      result[pos] = visitedNodes.getOrDefault(i, -1);
    }
    return result;
  }
}
