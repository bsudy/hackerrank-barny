package org.moresbycoffee.hackerrank.algorithms.dynamicprogramming;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/fibonacci-modified
 */
public class FibonacciModified {

  public static void main(final String[] args) {
    try(final Scanner scanner = new Scanner(System.in)) {
      int[] input = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

      System.out.println(new FibonacciModified().fibonacci(BigInteger.valueOf(input[0]), BigInteger.valueOf(input[1]), input[2]));
    }

  }

  public BigInteger fibonacci(BigInteger t1, BigInteger t2, int n) {
    final BigInteger t3 = t1.add(t2.pow(2));
    if (n == 3) {
      return t3;
    } else {
      return fibonacci(t2, t3, n - 1);
    }
  }
}
