package org.moresbycoffee.hackerrank.ai.machinelearning;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by chssyb on 04/10/2015.
 */
public class PredictingHousePrices {


  public static void main(final String[] args) {
    try(final Scanner scanner = new Scanner(System.in)) {
      int[] line1 = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
      final int numOfFeatures = line1[0];
      final int numOfTrainingCases = line1[1];
      final double[][] training = IntStream.range(0, numOfTrainingCases).mapToObj(i -> Arrays.stream(scanner.nextLine().split(" ")).mapToDouble(Double::parseDouble).toArray()).toArray(double[][]::new);

      final double[][] features = new double[training.length][training[0].length - 1];
      final double[] results = new double[training.length];
      for (int r = 0; r < training.length; r++) {
        for (int c = 0; c < training[r].length - 1; c++) {
          features[r][c] = training[r][c];
        }
        results[r] = training[r][training[r].length - 1];
      }

      final LinearRegression lr = LinearRegression.training(new double[numOfFeatures + 1], 0.01, 100000, features, results);

      int numOfTestCases = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray()[0];

      for (int i = 0; i < numOfTestCases; i++) {
        System.out.println(
            lr.predict(Arrays.stream(scanner.nextLine().split(" ")).mapToDouble(Double::parseDouble).toArray()));
      }
    }

  }

  public static class LinearRegression {

    private double[] parameters;

    public LinearRegression(double[] parameters) {
      this.parameters = parameters;
    }

    public double[] parameters() {
      return parameters;
    }

    public double predict(double[] features) {
      double sum = parameters[0];
      for (int i = 0; i < features.length; i++) {
        final double a = parameters[i + 1] * features[i];
        sum += a;
      }
      return sum;
    }

    /**
     *
     * @param features
     * @param result
     * @return h function - parameters
     */
    public static LinearRegression training(final double[] initalParameterVector, final double learningRate, final int iterations, final double[][] features, final double[] result) {

      double[] parameters = initalParameterVector;
      final double[][] featureSet = MathOperations.prependMatrixWithColumn(MathOperations.constantColumn(features.length, 1d), features);

      for (int i = 0; i < iterations; i++) {
        parameters = calculateNewParameters(parameters, learningRate, featureSet, result, hypothesis(parameters, featureSet));
      }

      return new LinearRegression(parameters);

    }

    private static double[] calculateNewParameters(
        final double[] parameters, final double learningRate, final double[][] featureSet, final double[] result, final double[] hypothesis) {
      final double[] newParameters = new double[parameters.length];
      for (int j = 0; j < parameters.length; j++) {
        final double bias = calculateBiasFor(result, featureSet, hypothesis, j);
        newParameters[j] = parameters[j] - 1d/((double)featureSet.length) * learningRate * bias;
      }
      return newParameters;
    }

    private static double calculateBiasFor(double[] result, double[][] featureSet, double[] hypothesis, int j) {
      double bias = 0;

      for (int l = 0; l < hypothesis.length; l++) {
        final double a = hypothesis[l] - result[l];
        bias += a * featureSet[l][j];
      }
      return bias;
    }

    private static double calculateError(double[] hypothesis, double[] result) {
      assert hypothesis.length > 0;
      assert hypothesis.length == result.length;

      double sum = 0;
      for (int i = 0; i < hypothesis.length; i++) {
        final double a = hypothesis[i] - result[i];
        sum += a * a;
      }
      return sum/(2 * hypothesis.length);
    }

    private static double[] hypothesis(final double[] parameters, final double[][] features) {
      return Arrays.stream(features).mapToDouble(feature -> MathOperations.multiply(feature, parameters)).toArray();
    }
  }

  public static class MathOperations {

    public static double multiply(double[] a, double[] b) {
      assert a.length > 0;
      assert a.length == b.length;

      double result = 0;
      for (int i = 0; i < a.length; i++) {
        result += a[i] * b[i];
      }
      return result;
    }

    public static double[][] prependMatrixWithColumn(double[] column, double[][] matrix) {
      final double[][] result = new double[matrix.length][matrix[0].length + 1];
      for (int r = 0; r < matrix.length; r++) {
        result[r][0] = column[r];
        for (int c = 0; c < matrix[r].length; c++) {
          result[r][c + 1] = matrix[r][c];
        }
      }
      return result;
    }

    public static double[] constantColumn(final int size, final double constant) {
      final double[] result = new double[size];
      Arrays.fill(result, constant);
      return result;
    }

  }

}
