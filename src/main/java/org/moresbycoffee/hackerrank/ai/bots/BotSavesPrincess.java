package org.moresbycoffee.hackerrank.ai.bots;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.IntStream.range;

/**
 * https://www.hackerrank.com/challenges/saveprincess
 */
public class BotSavesPrincess {

  public static void main(final String[] args) {
    try(final Scanner scanner = new Scanner(System.in)) {
      final int size = Integer.parseInt(scanner.nextLine());
      final char[][] grid = times(size, () -> scanner.nextLine().toCharArray()).toArray(char[][]::new);

      final Move[] moves = new BotSavesPrincess(grid).howToSavePrincess();

      for (final Move move : moves) {
        System.out.println(move);
      }
    }
  }

  public static <T> Stream<T> times(int n, Supplier<T> supplier) {
    return range(0, n).mapToObj(i -> supplier.get());
  }

  private final char[][] grid;

  public BotSavesPrincess(char[][] grid) {
    this.grid = grid;
  }


  public Move[] howToSavePrincess() {

    final Position princess = findThePrincessInTheCorners();
    final Position myself = positionOfMine();

    final int horizonalMoves = myself.x - princess.x;
    final int verticalMoves = myself.y - princess.y;

    final List<Move> result = new ArrayList<Move>();

    if (verticalMoves > 0) {
      result.addAll(range(0, verticalMoves).mapToObj(i -> Move.UP).collect(Collectors.toList()));
    } else if (verticalMoves < 0) {
      result.addAll(range(0, Math.abs(verticalMoves)).mapToObj(i -> Move.DOWN).collect(Collectors.toList()));
    }
    if (horizonalMoves > 0) {
      result.addAll(range(0, horizonalMoves).mapToObj(i -> Move.LEFT).collect(Collectors.toList()));
    } else if (horizonalMoves < 0) {
      result.addAll(range(0, Math.abs(horizonalMoves)).mapToObj(i -> Move.RIGHT).collect(Collectors.toList()));
    }

    return result.toArray(new Move[] {});
  }

  private Position positionOfMine() {
    final int size = grid.length;

    final int half = Math.floorDiv(size, 2);

    final Position pos = new Position(half, half);

    if (isMyPosition(pos)) {
      return pos;
    }

    throw new IllegalStateException("Myself is not in the grid In my pos: " + grid[pos.y][pos.x]);
  }

  private Position findThePrincessInTheCorners() {
    final int size  = grid.length;

    for (final Position pos : new Position[] { POS(0, 0), POS(0, size -1), POS(size - 1, 0), POS(size -1, size -1) }) {
      if (isPrincessAtPosition(pos)) {
        return  pos;
      }
    }

    throw new IllegalStateException("Princess is not in the grid");
  }

  private boolean isPrincessAtPosition(final Position position) {
    return grid[position.y][position.x] == 'p';
  }

  private boolean isMyPosition(final Position position) {
    return grid[position.y][position.x] == 'm';
  }

  private Position POS(final int x, final int y) {
    return new Position(x, y);
  }

  public Move nextMove(final Position myPosition) {

    final Position princessPosition = findThePrincessInTheGird();


    final int horizonalMoves = myPosition.x - princessPosition.x;
    final int verticalMoves = myPosition.y - princessPosition.y;

    final List<Move> result = new ArrayList<Move>();

    if (verticalMoves > 0) {
      return Move.UP;
    } else if (verticalMoves < 0) {
      return Move.DOWN;
    }
    if (horizonalMoves > 0) {
      return Move.LEFT;
    } else if (horizonalMoves < 0) {
      return Move.RIGHT;
    }
    return null;
  }

  private Position findThePrincessInTheGird() {
    for (int i = 0; i < grid.length; i++) {
      for (int j = 0; j < grid.length; j++) {
        if (grid[i][j] == 'p') {
          return new Position(j, i);
        }
      }
    }
    throw new IllegalStateException("Princess is not in the grid");
  }

  static class Position {

    Position(final int x, final int y) {
      this.x = x;
      this.y = y;
    }

    final int x;
    final int y;
  }


  public enum Move {
    LEFT,
    RIGHT,
    UP,
    DOWN
  }
}
