package org.moresbycoffee.hackerrank.ai.bots;

import org.moresbycoffee.hackerrank.ai.bots.BotSavesPrincess.Move;

import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.stream.IntStream.range;

/**
 * Created by chssyb on 04/10/2015.
 */
public class BotSavesPrincess2 {

  public static void main(final String[] args) {
    try(final Scanner scanner = new Scanner(System.in)) {
      final int size = Integer.parseInt(scanner.nextLine());
      final int[] myPos = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
      final char[][] grid = times(size, () -> scanner.nextLine().toCharArray()).toArray(char[][]::new);
      final Move move = new BotSavesPrincess(grid).nextMove(new BotSavesPrincess.Position(myPos[0], myPos[1]));
      System.out.println(move);
    }
  }

  public static <T> Stream<T> times(int n, Supplier<T> supplier) {
    return range(0, n).mapToObj(i -> supplier.get());
  }
}
