package org.moresbycoffee.hackerrank;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * https://www.hackerrank.com/challenges/insertionsort2
 */
public class InsertionSort {

  public static void main(final String[] args) {
    try(final Scanner scanner = new Scanner(System.in)) {
      int n = scanner.nextInt();
      scanner.nextLine();
      int[] input = Arrays.stream(scanner.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();

      new InsertionSort().sortInPlace(input);
    }

  }


  public int[] sortInPlace(int[] array) {

    for (int end = 1; end < array.length; end++) {
      int v = array[end];

      for (int i = end; i >= 0; i--) {
        if (i == 0) {
          array[i] = v;
          System.out.println(Arrays.stream(array).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
        } else if (array[i - 1] > v) {
          array[i] = array[i - 1];
        } else {
          array[i] = v;
          System.out.println(Arrays.stream(array).mapToObj(Integer::toString).collect(Collectors.joining(" ")));
        }

        if (array[i] == v)
          break;
      }
    }
    return array;
  }
}
