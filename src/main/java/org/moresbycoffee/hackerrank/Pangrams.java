package org.moresbycoffee.hackerrank;

import java.util.BitSet;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/pangrams
 */
public class Pangrams {

  public static void main(final String[] args) {
    try(final Scanner scanner = new Scanner(System.in)) {
      final String line = scanner.nextLine();
      final boolean panagram = new Pangrams().isPangram(line);
      System.out.println(panagram ? "pangram" : "not pangram");
    }
  }


  public boolean isPangram(String s) {
    final String allLowerCase = s.toLowerCase();
    BitSet bitSet = new BitSet();
    for (int i = 0; i < allLowerCase.length(); i++) {
      final char ch = allLowerCase.charAt(i);
      bitSet.set(ch, true);
    }

    for (char ch = 'a'; ch <= 'z'; ch++) {
      if (!bitSet.get(ch)) {
        return false;
      }
    }
    return true;
  }
}

