package org.moresbycoffee.hackerrank.algorithms.graphs;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class BFSShortestReachTest {

  public BFSShortestReach sut = new BFSShortestReach();

  @Test public void should_find_shortest_path_in_single_not_graph() throws Exception {
    assertArrayEquals(new int[]{}, sut.shortestReach(1, new int[][] {}, 1));
  }

  @Test public void should_find_route_between_two_nodes() throws Exception {

    assertArrayEquals(new int[]{6}, sut.shortestReach(2, new int[][] { { 1, 2 } }, 1));
    assertArrayEquals(new int[] { -1 }, sut.shortestReach(2, new int[][] {}, 1));
  }

  @Test public void should_find_shortest_path_in_uncoupled_graph() throws Exception {

    assertArrayEquals(new int[] { 6, 6, -1 }, sut.shortestReach(4, new int[][] { {1,2}, {1,3} }, 1));
    assertArrayEquals(new int[] { 6, 6, -1 }, sut.shortestReach(4, new int[][] { {1,2}, {1,3} }, 1));
  }

  @Test public void should_find_shortest_path() throws Exception {

    assertArrayEquals(new int[] { 6, 6, -1 }, sut.shortestReach(4, new int[][] { {1,2}, {1,3} }, 1));
    assertArrayEquals(new int[] { 6, 12, 18 }, sut.shortestReach(4, new int[][] {{1,2},{2,3},{3,4}}, 1));
  }

}