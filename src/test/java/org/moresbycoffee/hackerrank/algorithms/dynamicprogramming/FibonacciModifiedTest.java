package org.moresbycoffee.hackerrank.algorithms.dynamicprogramming;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

import static java.math.BigInteger.*;
import static org.junit.Assert.*;

public class FibonacciModifiedTest {

  private FibonacciModified sut = new FibonacciModified();

  @Test public void should_calculate_fibonacci_numbers() throws Exception {

    // 0 1 1 2 5 27
    // 1 2 3 4 5 6

    assertEquals(valueOf(5), sut.fibonacci(ZERO, ONE, 5));
    assertEquals(valueOf(27), sut.fibonacci(ZERO, ONE, 6));
    assertEquals(new BigInteger("84266613096281243382112"), sut.fibonacci(ZERO, ONE, 10));

    // 0 0 0 0 0 0 0
    // 1 2 3 4 5 6
    assertEquals(valueOf(0), sut.fibonacci(ZERO, ZERO, 5));

    // 1 1 2 5 9
    // 1 2 3 4 5 6
    assertEquals(valueOf(27), sut.fibonacci(ONE, ONE, 5));
  }
}