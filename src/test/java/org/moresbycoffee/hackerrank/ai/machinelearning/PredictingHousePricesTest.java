package org.moresbycoffee.hackerrank.ai.machinelearning;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by chssyb on 06/10/2015.
 */
public class PredictingHousePricesTest {

  @Test public void should_should_calculate() throws Exception {

    double[][] features = { { 0.18, 0.89 }, { 1.0, 0.26 }, { 0.92, 0.11 }, { 0.07, 0.37 }, { 0.85, 0.16 },
        { 0.99, 0.41 }, { 0.87, 0.47 } };

    double[] results = {109.85,
        155.72,
        137.66,
        76.17,
        139.75,
        162.6,
        151.77};

    PredictingHousePrices.LinearRegression linearRegression = PredictingHousePrices.LinearRegression
        .training(new double[] { 0, 0, 0 }, 0.01, 100000, features, results);

    assertEquals(105.22, linearRegression.predict(new double[] {0.49, 0.18}), 0.01);
    assertEquals(142.68, linearRegression.predict(new double[] {0.57, 0.83}), 0.01);
    assertEquals(132.94, linearRegression.predict(new double[] {0.56, 0.64}), 0.01);
    assertEquals(129.71, linearRegression.predict(new double[] {0.76, 0.18}), 0.01);
  }
}