package org.moresbycoffee.hackerrank.ai.machinelearning;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by chssyb on 05/10/2015.
 */
public class MathOperationsTest {

  @Test public void should_should_prepand_matix_with_column() throws Exception {

    double[][] result = PredictingHousePrices.MathOperations.prependMatrixWithColumn(new double[]{1, 4}, new double[][]{{2, 5}, {3, 6}});

    for (int r = 0; r < result.length; r++) {
      for (int c = 0; c < result.length; c++) {
        result[r][c] = r * 3 + c + 1;
      }
    }


  }
}