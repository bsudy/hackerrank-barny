package org.moresbycoffee.hackerrank.ai.machinelearning;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LinearRegressionBuilderTest {

  @Test public void should_calculate_simple_parameters() throws Exception {

    final double[] initalParameterVector = {0,1};
    final double learningRate = 0.001;
    final int iterations = 1;
    final double[][] features = {{1}};
    final double[] result = {1};


    PredictingHousePrices.LinearRegression linearRegression = PredictingHousePrices.LinearRegression
        .training(initalParameterVector, learningRate, iterations, features, result);

    assertArrayEquals(new double[] {0, 1}, linearRegression.parameters(), 0.00000000001);

  }

  @Test public void should_calculate_simple_parameters2() throws Exception {

    final double[] initalParameterVector = {0, 1};
    final double learningRate = 0.001;
    final int iterations = 1000;
    final double[][] features = {{1}, {2}};
    final double[] result = {1, 2};

    PredictingHousePrices.LinearRegression linearRegression = PredictingHousePrices.LinearRegression
        .training(initalParameterVector, learningRate, iterations, features, result);

    assertArrayEquals(new double[] {0 ,1}, linearRegression.parameters(), 0.00000000001);

  }

  @Test public void should_calculate_simple_parameters3() throws Exception {

    final double[] initalParameterVector = {0, 1};
    final double learningRate = 0.1;
    final int iterations = 100000;
    final double[][] features = {{1}, {2}};
    final double[] result = {2, 4};

    PredictingHousePrices.LinearRegression linearRegression = PredictingHousePrices.LinearRegression
        .training(initalParameterVector, learningRate, iterations, features, result);

    assertArrayEquals(new double[] {0, 2}, linearRegression.parameters(), 0.00000000001);

  }

  @Test public void should_calculate_simple_parameters4() throws Exception {

    final double[] initalParameterVector = {0, 0, 0};
    final double learningRate = 0.1;
    final int iterations = 1;
    final double[][] features = {{1, 1}, {2, 2}, {2, 3}, {1, 2}, {3, 5}};
    final double[] result = {5, 4, 4, 3, 2};

    PredictingHousePrices.LinearRegression linearRegression = PredictingHousePrices.LinearRegression
        .training(initalParameterVector, learningRate, iterations, features, result);

    assertArrayEquals(new double[] {0.36, 0.6, 0.82}, linearRegression.parameters(), 0.00000000001);

  }

}