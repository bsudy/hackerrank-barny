package org.moresbycoffee.hackerrank.ai.bots;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.moresbycoffee.hackerrank.ai.bots.BotSavesPrincess.Move;

import static org.junit.Assert.*;

public class BotSavesPrincessTest {




  @Test public void should_should_save_the_princess_in_simple_3x3_matrix() throws Exception {

    final BotSavesPrincess sut = new BotSavesPrincess(new char[][] { {'-','-','-'},
        {'-','m','-'},
        {'p','-','-'}});
    final Move[] moves = sut.howToSavePrincess();

    assertArrayEquals(new Move[] { Move.DOWN, Move.LEFT }, moves);
  }

  @Test public void should_return_the_next_move_to_save_the_princess() throws Exception {

    final BotSavesPrincess sut = new BotSavesPrincess(new char[][] { { '-', '-', '-', '-', '-' },
        { '-', '-', '-', '-', '-' },
        { 'p', '-', '-', 'm', '-' },
        { '-', '-', '-', '-', '-' },
        { '-', '-', '-', '-', '-' } });

    final Move nextMove = sut.nextMove(new BotSavesPrincess.Position(3, 2));

    assertEquals(Move.LEFT, nextMove);
  }

  @Test public void should_return_the_next_move_to_save_the_princess2() throws Exception {

    final BotSavesPrincess sut = new BotSavesPrincess(new char[][] { { '-', '-', '-', '-', 'm' },
        { '-', '-', '-', '-', '-' },
        { '-', '-', '-', '-', 'p' },
        { '-', '-', '-', '-', '-' },
        { '-', '-', '-', '-', '-' } });

    final Move nextMove = sut.nextMove(new BotSavesPrincess.Position(4, 0));

    assertEquals(Move.DOWN, nextMove);
  }

}