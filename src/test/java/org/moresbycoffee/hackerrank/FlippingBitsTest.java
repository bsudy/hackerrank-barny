package org.moresbycoffee.hackerrank;

import org.junit.Before;
import org.junit.Test;

import static java.lang.Integer.parseUnsignedInt;
import static java.lang.Integer.toUnsignedString;
import static org.junit.Assert.*;

/**
 * Created by chssyb on 01/10/2015.
 */
public class FlippingBitsTest {

  private FlippingBits sut;

  @Before
  public void setUp() throws Throwable {
    sut = new FlippingBits();
  }

  @Test
  public void test() throws Throwable {

    assertEquals("2147483648", toUnsignedString(sut.flip(parseUnsignedInt("2147483647"))));
    assertEquals("4294967294", toUnsignedString(sut.flip(parseUnsignedInt("1"))));
    assertEquals("4294967295", toUnsignedString(sut.flip(parseUnsignedInt("0"))));


  }

}