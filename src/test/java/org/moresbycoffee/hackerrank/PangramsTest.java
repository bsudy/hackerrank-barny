package org.moresbycoffee.hackerrank;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by chssyb on 01/10/2015.
 */
public class PangramsTest {

  private Pangrams sut;

  @Before
  public void setUp() {
    sut = new Pangrams();
  }

  @Test
  public void testSuccess() throws Throwable {
    assertTrue(sut.isPangram("The quick brown fox jumps over the lazy dog"));
    assertTrue(sut.isPangram("We promptly judged antique ivory buckles for the next prize"));
  }

  @Test
  public void testFailure() throws Throwable {
    assertFalse(sut.isPangram(""));
    assertFalse(sut.isPangram("Barny"));

  }


}