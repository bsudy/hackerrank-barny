package org.moresbycoffee.hackerrank;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class InsertionSortTest {

  InsertionSort sut = new InsertionSort();

  @Test public void should_sort_one_element() throws Exception {

    assertArrayEquals(new int[] { 1 }, sut.sortInPlace(new int[]{1}));
  }

  @Test public void should_sort_the_last_element() throws Exception {

    assertArrayEquals(new int[]{1,2,3,4}, sut.sortInPlace(new int[]{1,3,4,2}));
  }

  @Test public void should_sort_the_array() throws Exception {

    assertArrayEquals(new int[]{1,2,3,4,5,6}, sut.sortInPlace(new int[]{1,4,3,5,6,2}));
  }
}